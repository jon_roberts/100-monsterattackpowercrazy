//
//  Inventory.swift
//  RGPex
//
//  Created by Jon Roberts on 24/09/2015.
//  Copyright © 2015 Dinoware. All rights reserved.
//

import Foundation

class Inventory{


    private var _gold: Int = 0
    private var _weapons: [Weapon]
    private var _potions: Int = 3
    private var _items: [Item]
   
    
    var gold: Int {
        get{
            return _gold
        }
        set {
            _gold += newValue
        }
    }
    var pots: Int {
        get{
            return _potions
        }
        set {
            _potions++
        }
    }
    
    var weapons: [Weapon]{
        get{
            return _weapons
        }
    }
    var items: [Item]{
        get{
            return _items
        }
    }



    init(){
        _gold = 100
        _potions = 3
        _weapons = [Weapon]()
        _items = [Item]()
        
        
        
               
    }

    func addItem(item: Item){
        self._items.append(item)
    }
 
    func removeItem(item: Item){
       
        if let index = _items.indexOf(item)  {  //find (_items,item)) != nil){
            _items.removeAtIndex(index)
        }
    }
  
    func addWeapon(weapon: Weapon){
        _weapons.append(weapon)
    }
    
    func dropWeapon(weapon: Weapon){
        if let index = _weapons.indexOf(weapon)  {  //find (_items,item)) != nil){
            _weapons.removeAtIndex(index)
        }
    
    }
    
    func printInventory(){
        print(" Gold: \(gold) Pots: \(pots)")
        for w in weapons {
            w.printWeapon()
        }
        for i in items {
            i.printItem()
        }
    }
    
    
}