//
//  ItemGenerator.swift
//  RGPex
//
//  Created by Jon Roberts on 25/09/2015.
//  Copyright © 2015 Dinoware. All rights reserved.
//

import Foundation

class ItemGenerator {
    
    
    private var _item: Item
    
    var item: Item {
        get {
            return _item
        }
    }
    
    
    init(lvl: Int){

        _item = Item(name: "Item Name", desc: "Desc", weight: 100, value: 100)
        _item = self.generateRandomItemBasedOnLvl(lvl)
    
        
        
    
    
    }
    
    func generateRandomItemBasedOnLvl(lvl: Int) -> Item{
        
        let name:String = generateName()!
        
        let desc:String = generateDescription(name)
          
        
        let weight = Utils.getRandomNumber(1, maxValue: lvl * 3) + lvl * 2
        
        let value = (Utils.getRandomNumber(1, maxValue: lvl * 5) + lvl * 10)
        
        let item = Item(name: name, desc: desc, weight: weight, value: value)
        
        return item
        
        
    }
    
    func arrayFromContentsOfFileWithName(fileName: String) -> [String]? {
        guard let path = NSBundle.mainBundle().pathForResource(fileName, ofType: "txt") else {
            return nil
        }
        
        do {
            let content = try String(contentsOfFile:path, encoding: NSUTF8StringEncoding)
            return content.componentsSeparatedByString("\n")
        } catch _ as NSError {
            return nil
        }
    }

    func generateDescription(name: String) -> String {
        
        return "Random Desc"
        
    }
    
    
    func generateName() -> String?{
        
        let items:[String] = arrayFromContentsOfFileWithName("magic_item_names")!
        
        let rnd = Utils.getRandomNumber(0, maxValue: (items.count))
            return items[rnd]
        
        
    }
    
    
        
    
    
    
    
    
}