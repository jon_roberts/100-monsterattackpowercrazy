//
//  Weapon.swift
//  RGPex
//
//  Created by Jon Roberts on 24/09/2015.
//  Copyright © 2015 Dinoware. All rights reserved.
//

import Foundation

func == (lhs: Weapon, rhs: Weapon) -> Bool {
    if lhs.id == rhs.id && lhs.name == rhs.name {
        return true
    }
    return false
}

class Weapon : Equatable {

    private var _name: String
    private var _description: String
    private var _damage: Int
    private var _attackPower: Int
    private var _duribility: Int
   
    private var _weight: Int
    private var _id: String
    
    
    
    var name : String {
        get {
            return _name
        }
        set {
            _name = newValue
        }
    }
    
    var description : String {
        get {
            return _description
        }
        set {
            _description = newValue
        }
    }
    
    
    var damage : Int {
        get {
            return _damage
        }
        set {
            _damage = newValue
        }
    }
    var ap : Int {
        get {
            return _attackPower
        }
        set {
            _attackPower = newValue
        }
    }
    var dur : Int {
        get {
            return _duribility
        }
        set {
            _duribility = newValue
        }
    }
    
    
    var weight : Int {
        get {
            return _weight
        }
        set {
            _weight = newValue
        }
    }
    
    var id: String {
        get{
            return _id
        }
    }

    init(){
        
        _name = "Rusty Dagger"
        _description = "This is a very blunt and rusty dagger, it is slightly better than your fist"
        _damage = 1
        _attackPower = 1
        _duribility = 10
        _weight = 1
        _id  = NSUUID().UUIDString
    }
    
    init (name: String, desc: String, dam: Int, ap: Int, dur: Int, weight: Int){
        
        _name = name
        _description = desc
        _damage = dam
        _attackPower = ap
        _duribility = dur
        _weight = weight
        _id  = NSUUID().UUIDString
       
    }
    
    
    func printWeapon() {
        
        print ("\(name) \n \(description)\n d: \(damage) ap: \(ap) dur: \(dur) w: \(weight) \n \(id) \n")
        
        
        
    }

}