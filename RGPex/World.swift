//
//  World.swift
//  RGPex
//
//  Created by Jon Roberts on 25/09/2015.
//  Copyright © 2015 Dinoware. All rights reserved.
//

import Foundation

class world{
    
    
    let MOB_BASE = 10
    let MAX_MISSES = 5
    let BASE_POTION_HEALS = 150
    let BASE_POTION_PRICE = 25
    
    
    
    
    
    private var _round = 1
    private var _maxMobs = 2
    private var _currentMonsterNo = 0
    private var _currentMisses = 0

    private var _pBaseAttack = 10
    private var _pSAttackCost = 25
    private var _pSAttackRegen = 10
    
    private var _startingPotions = 3
    private var _potionPrice = 25
    private var _potionHeals = 150
    
    private var _player: Character
    
    private var _pStats: [String:Int] = ["hpScale":10, "defScale":15, "attackScale":15, "hpBase":1000, "sAttBase":25]
    
    private var _mobStats: [String:Double] = ["hpScale": 1.1, "defScale":0.7, "attackScale":2.5]
    
    
    private var _currentMob: Mob
    private var _nextMob: Mob
    private var _previousMob: Mob
    
    private var _mobsForRound: [Mob]
    
    var rnd: Int {
        get{
            return _round
        }
        set {
            _round = newValue
        }
    }
    var maxMobs:Int {
        get{
            return _maxMobs
        }
        set {
            _maxMobs = newValue
        }
    }
    
    var currentMonsterNo: Int {
        get {
            return _currentMonsterNo
        }
        set {
            _currentMonsterNo = newValue
        }
    }
    
    var currentMisses: Int {
        get {
            return _currentMisses
        }
        set {
            _currentMisses = newValue
        }
    }
    
    var pBaseAttack: Int {
        get {
            return _pBaseAttack
        }
        
    }
    var pSAttackCost: Int {
        get {
            return _pSAttackCost
        }
    }
    var pSAttackRegen: Int {
        get {
            return _pSAttackRegen
        }
    }
    
    var startingPotions: Int {
        get {
            return _startingPotions
        }
    }
    
    var _potionPrice: Int {
        
    }
    
    
    init(){
    
    
    
    
    
    }
    
}