//
//  Item.swift
//  RGPex
//
//  Created by Jon Roberts on 24/09/2015.
//  Copyright © 2015 Dinoware. All rights reserved.
//

import Foundation

func == (lhs: Item, rhs: Item) -> Bool {
    if lhs.id == rhs.id && lhs.name == rhs.name {
        return true
    }
        return false
}
class Item : Equatable {

    private var _name: String
    private var _description: String?
    private var _weight: Int
    private var _value: Int
    private var _id: String
    
    
    
    init(name: String, desc: String, weight: Int, value: Int){
        
        _name = name
        _description = desc
        _weight = weight
        _value = value
        _id  = NSUUID().UUIDString

    }
    
    
    var name: String {
        get {
            return _name
            
        }
        set {
            _name = newValue
        }
    }

    var desc: String? {
        get {
            return _description
            
        }
        set {
            _description = newValue
        }
    }

    var weight: Int {
        get {
            return _weight
        }
        set {
            _weight = newValue
        }
    }
    var value: Int {
        get {
            return _value
        }
        set {
            _value = newValue
        }
    }
    var id: String {
        get {
            return _id
        }
    }
    
    func printItem(){
        print("\(name)\n \(desc) \n weight: \(weight) value: \(value) \n \(id)")
    
    }
    
    

}