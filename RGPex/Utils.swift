//
//  Utils.swift
//  RGPex
//
//  Created by Jon Roberts on 24/09/2015.
//  Copyright © 2015 Dinoware. All rights reserved.
//

import Foundation

class Utils{
    
    init(){}

    static func getRandomNumber(minValue: Int, maxValue: Int) -> Int {
        
        let rnd = Int(arc4random_uniform(UInt32(maxValue - minValue))) + minValue
        return rnd
        
    }
}