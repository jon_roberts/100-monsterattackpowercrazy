//
//  ViewController.swift
//  RGPex
//
//  Created by Jon Roberts on 23/09/2015.
//  Copyright © 2015 Dinoware. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var playerRightImg: UIImageView!
    @IBOutlet var playerLeftImg: UIImageView!
    
    @IBOutlet var attackLeft: UIButton!
    @IBOutlet var attackRight: UIButton!
    
    @IBOutlet var infoLabel: UILabel!
    
    @IBOutlet var leftHpLbl: UILabel!
    
    @IBOutlet var rightHpLbl: UILabel!
    
    @IBOutlet var damageLbl: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       
        let player = Character(name: "Fred", type: "Warrior")

        player.printChar()
        
        player.lootItem(generateRandomItem())
         player.lootItem(generateRandomItem())
        let w = Weapon(name: "Sword", desc: "A Sword", dam: 10, ap: 10, dur: 100, weight: 1)
        player.inv.addWeapon(w)
        let w2 = Weapon(name: "A 2nd Sword", desc: "A better Sword", dam: 110, ap: 10, dur: 100, weight: 1)
        player.inv.addWeapon(w2)
        player.lootGold(100)
        player.lootPotion()
         player.lootItem(generateRandomItem())
        player.printChar()
        
        let itemToDrop = player.inv.items[1]
        player.dropItem(itemToDrop)
        
        let wToDrop = player.inv.weapons[1]
        player.sellWeap(wToDrop)
        player.printChar()
        
        let igen:Item = ItemGenerator(lvl: 1).item
        
                let igen2:Item = ItemGenerator(lvl: 3).item
        player.inv.addItem(igen)
            player.inv.addItem(igen2)
        player.printChar()
        
     
        var m = Mob()
        m.printChar()
        
        
    }
    
    
    func generateRandomItem() -> Item{
    
        // init(name: String, desc: String, weight: Int, value: Int){
        
    return Item(name: "Old Rock", desc: "This is really nothing", weight: 1, value: 0)
    
    }
    
    func generateRandomWeapon() -> Weapon{
    
        return Weapon()
    }
    
    
    func generateWeaponName(type: String) -> String {

    
        
        let joiner = ["of", "the", "of the"]
        let index = rnd(0,max: joiner.count)
       
        let join = joiner[index]
        
        // var join = joiner[ Math.floor((Math.random() * 3 - 1) + 1) ];
        
        let wName = ["Decapitation", "Eradication", "Avenging", "Torture", "Horror", "Pain", "Demolition", "Deprivation", "Judgement", "Disease"];
        
        let wName2 = ["Awful", "Intimate", "Desperate", "Conclusive", "Exhaustive", "Erratic", "Enigmatic", "Sarcastic"]
        
        let wName3 = ["Potent", "Suppressive", "Death-dealing", "Irritating", "Explosive"]
    
        
        
        let w3 = wName3[rnd(1, max: wName3.count)];
        let w2 = wName2[rnd(1, max: wName2.count)];
        let w = wName[rnd(1, max:  wName.count)];
        
        let name = w3 + " " + type + " " + join + " " + w2 + " " + w
        
        return name;
        }

    
    func rnd(min:Int, max:Int) -> Int{
            return Utils.getRandomNumber(min, maxValue: max)
    }
    
    
    @IBAction func onAttackPressed(btn: UIButton!) {
     
    }
    
    
    
}

