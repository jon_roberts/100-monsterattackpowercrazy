//
//  Character.swift
//  RGPex
//
//  Created by Jon Roberts on 23/09/2015.
//  Copyright © 2015 Dinoware. All rights reserved.
//

import Foundation

class Character {
    
    var _name: String
    var _hp: Int
    var _attack: Int
    var _def: Int
    var _level: Int
    var _exp: Int
    var _charType: String
    
    var _inv: Inventory
    var _weapon: Weapon
    
    
    
    
    let BASE_HP = 100
    let BASE_ATT = 5
    let BASE_DEF = 5
    
   
    
    
    init(name: String, type: String){
        _name = name
        _charType = type
        
        _hp = 0
        _attack = 0
        _def = 0
        _level = 1
        _exp = 0
        _weapon = Weapon()
        _inv = Inventory()
        
        _hp = self.generateInitialHp(_level)
        _attack = self.generateInitialAttack(_level)
        _def = self.generateInitialDef(_level)
      
        
        
    }
    

    
    
    
    
    var name: String {
        get {
            return _name
        }
        set {
            _name = newValue
        }
    }
    
    
    var hp: Int {
        set {
            _hp = newValue
            
        }
        get {
            return _hp
        }
    }
    
    var attack: Int {
        get {
            return _attack
        }
        set {
            _attack = newValue
        }
    }
    var def: Int {
        get {
            return _def
        }
        set {
            _def = newValue
        }
    }
    
    var level: Int {
        get {
            return _level
            
        }
        set {
            if (newValue > 1){
                _level++
            } else{
                _level++
            }
            
        }
    }
    
    var exp: Int {
        get {
            return _exp
        }
        set {
            _exp = newValue
        }
    }
    
    var charType: String {
        get {
            return _charType
        }
        
    }
    
    var inv: Inventory{
        get{
        
            return _inv
    
        }
    }
    
    var weapon: Weapon{
        get{
            return _weapon
        }
    }
    
    
    func attemptAttack(){}
    
    func attemptDefend(){}
    
    func doAttack(){}
    
    func calcAp(){}
    
    func calcDef(){}
    
    func calcDam(){}
    
   

    
    
    
    
    func isAlive() -> Bool {
        return true
        }

    
    func lootItem(item: Item){
    
        _inv.addItem(item)
        
        
    }
    
    func lootGold(gold: Int){
        inv.gold = gold
    }
   
    func lootPotion(){
        inv.pots++
    }
    
    func swapWeapon(w: Weapon){
        
        
        let tmpweapon = _weapon
        
        _inv.addWeapon(tmpweapon)
        
        _weapon = w
        
    }
    
    func dropItem(item:Item){
        _inv.removeItem(item)
    }
    func sellWeap(w: Weapon){
        _inv.dropWeapon(w)
    }
    
    func generateInitialHp(multiplier: Int) -> Int{
        
        let h = BASE_HP + (getRandomNumber(50,maxValue: 100) * multiplier)
        return h
    }
    
    func generateInitialAttack(multiplier: Int) -> Int {
        let a = BASE_ATT + (getRandomNumber(10, maxValue: 20) * multiplier)
        return a
    }
    
    func generateInitialDef(multiplier: Int) -> Int {
        let d = BASE_DEF + (getRandomNumber(5, maxValue: 10) * multiplier)
        return d
        
    }
    
    
    func getRandomNumber(minValue: Int, maxValue: Int) -> Int {
        
        return Utils.getRandomNumber( minValue, maxValue: maxValue)
        
    }

    func printChar(){
        print ("\(name) \n hp: \(hp) ap: \(attack) def: \(def) lvl: \(level) xp: \(exp) " )
        print ("\(charType)")
        inv.printInventory()
    
    
    }
    
    
}